call plug#begin('~/.vim/plugged')

"Git
Plug 'tpope/vim-fugitive'

"Linting etc
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"Git
Plug 'tpope/vim-fugitive'

"Navigation
Plug 'justinmk/vim-sneak'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'

"Fuzzy Finder
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

"Easy life
Plug 'jiangmiao/auto-pairs'
Plug 'preservim/nerdcommenter'

"DAP debugging live
Plug 'mfussenegger/nvim-dap'

"Themes
Plug 'ryanoasis/vim-devicons' "adds icons in nerdtree
Plug 'vim-airline/vim-airline'
Plug 'morhetz/gruvbox'
Plug 'kshenoy/vim-signature'

call plug#end()


filetype plugin indent on    " required

"Syntax highlighting on
syntax on

"Initializations
"---------------------------

let g:airline_theme='gruvbox'
let g:colorizer_auto_color = 1
let g:colorizer_auto_filetype='css,html,lua,vim'

colorscheme gruvbox
set background=dark

"C++ linting via syntastic and cpplint
let g:syntastic_cpp_checkers = ['cpplint']
let g:syntastic_c_checkers = ['cpplint']
let g:syntastic_cpp_cpplint_exec = 'cpplint'
" The following two lines are optional. Configure it to your liking!
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0

"NerCommenter
filetype plugin on

"Autocomplete commands with menu
set wildmenu

"fzf 
set rtp+=/usr/local/opt/fzf
" Give more space for displaying messages.                                  (coc suggested)
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.                                          (coc suggested)
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time   (coc suggested)
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

"Spliting
set splitbelow
set splitright

" Enable folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za


"Enable lightline - cool bottom bar
set laststatus=2
"set noshowmode

"Tabstops for all
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent

"Lua specific settings
au BufNewFile,BufRead *.lua
    \ set tabstop=4
    \ | set shiftwidth=4

"HTML specific settings
au BufNewFile,BufRead *.html
    \ set tabstop=2
    \ | set shiftwidth=2

"CSS specific settings
au BufNewFile,BufRead *.css
    \ set tabstop=2
    \ | set shiftwidth=2

"Python specific settings
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ | set softtabstop=4
    \ | set shiftwidth=4
    \ | set textwidth=79
    \ | set expandtab
    \ | set autoindent
    \ | set fileformat=unix 
    \ | set encoding=utf-8


"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" set relativenumber
set number

let mapleader = ","
let g:user_emmet_leader_key=','
" set clipboard=unnamed

command! W :call MakeDirsAndSaveFile()
function! MakeDirsAndSaveFile()
  :silent !mkdir -p %:h
  :redraw!
  :write
endfunction

"MAPs and remaps
"---------------------------
"Normal mode Mappings

"Tab to cycle completions for coc

" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"


"hex highlighting
nmap <F2>           :ColorHighlight<CR>

    "copy and paste to system clipboard
nnoremap <leader>p "+p
nnoremap <leader>y "+y
nnoremap <leader>d "+d
vnoremap <leader>p "+p
vnoremap <leader>y "+y
vnoremap <leader>d "+d

    "turn off and on mouse
" nnoremap <leader>m :set mouse=a<CR>:set rnu<CR>:set nu<CR>
" nnoremap <leader>M :set mouse=""<CR>:set nornu<CR>:set nonu<CR>
" vnoremap <leader>m :set mouse=a<CR>:set rnu<CR>:set nu<CR>
" vnoremap <leader>M :set mouse=""<CR>:set nornu<CR>:set nonu<CR>
nnoremap <leader>m :set mouse=a<CR>:set nu<CR>
nnoremap <leader>M :set mouse=""<CR>:set nonu<CR>
vnoremap <leader>m :set mouse=a<CR>:set nu<CR>
vnoremap <leader>M :set mouse=""<CR>:set nonu<CR>

    "search selected
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

    "split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

    "tabs
nnoremap <C-t>n :tabnew<CR>
nnoremap <C-t>N <C-w>T
nnoremap <C-t>q :tabc<CR>
"Tab move left
nnoremap <C-t>m :-tabmove
"Tab move right
nnoremap <C-t>M :+tabmove
"Close all other tabs
nnoremap <C-t>o :tabonly<CR> 
nnoremap <C-t>r :tabdo

    "Diff
map <leader>, :diffget<CR>
map <leader>. :diffput<CR>

    "very magic search
map <leader>v/ /\v
    
    "NERD Tree
nnoremap <leader><C-n> :NERDTreeToggle<CR>

"---------------------------
"Visual Mode Mappings
    "Same as * in normal mode, but for selected items
vnoremap * y/<C-R>"<CR>

"---------------------------
"Insert mod Mappings

"---------------------------
"Plugins

"Auto-Pairs
let g:AutoPairsShortcutToggle = '<leader><C-p>'

"Disable auto comments (after newline etc)
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"Fuzzy Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"NERD commenter
    " Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
    " Use compact syntax for prettified multi-line comments
"let g:NERDCompactSexyComs = 1
    " Align line-wise comment delimiters flush left instead of following code indentation
"let g:NERDDefaultAlign = 'left'
    " Set a language to use its alternate delimiters by default
"let g:NERDAltDelims_java = 1
    " Add your own custom formats or override the defaults
"let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
    " Allow commenting and inverting empty lines (useful when commenting a region)
"let g:NERDCommentEmptyLines = 1
    " Enable trimming of trailing whitespace when uncommenting
"let g:NERDTrimTrailingWhitespace = 1

    " Enable NERDCommenterToggle to check all selected lines is commented or not 
"let g:NERDToggleCheckAllLines = 1


"Enable mouse support
set mouse=a

"COC settings

    " Switch to header
nmap <silent> <leader>h :CocCommand clangd.switchSourceHeader<CR>

    " Use tab for trigger completion with characters ahead and navigate.
    " NOTE: There's always complete item selected by default, you may want to enable
    " no select by `"suggest.noselect": true` in your configuration file.
    " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    " other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

    " Make <CR> to accept selected completion item or notify coc.nvim to format
    " <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

    " Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

    " Use `[g` and `]g` to navigate diagnostics
    " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

    " Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

    " Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

    " Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

    " Applying codeAction to the selected region.
    " Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

    " Remap keys for applying codeAction to the current buffer.
nmap <leader>qa  <Plug>(coc-codeaction)
    " Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

    " Run the Code Lens action on the current line.
nmap <leader>ql  <Plug>(coc-codelens-action)

    " Map function and class text objects
    " NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

    " Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

    " Use CTRL-S for selections ranges.
    " Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

    " Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

    " Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

    " Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

    " Add (Neo)Vim's native statusline support.
    " NOTE: Please see `:h coc-status` for integrations with external plugins that
    " provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

    " Mappings for CoCList
    " Show all diagnostics.
" nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
    " " Manage extensions.
" nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
    " " Show commands.
" nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
    " " Find symbol of current document.
" nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
    " " Search workspace symbols.
" nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
    " " Do default action for next item.
" nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
    " " Do default action for previous item.
" nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
    " " Resume latest coc list.
" nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
